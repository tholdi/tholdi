<?php
include_once '_debut.inc.php';
?>



    
</select>  

<form method="post" action="creerReservation.traitement.php">
<div class="row">
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon bgColorTheme minTextBox">Ville départ</span>
            <select name="nomVilleAller">

    <?php
    $lesVilles = listeVille();
    foreach ($lesVilles as $ville):
        ?>
        <option value = "<?php echo $ville["codeVille"]; ?>" > <?php echo $ville["nomVille"]; ?></option>

    <?php endforeach; ?>

</select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon bgColorTheme minTextBox">Ville arrivé</span>
            <select name="nomVilleRetour">

    <?php
    $lesVilles = listeVille();
    foreach ($lesVilles as $ville):
        ?>
        <option value = "<?php echo $ville["codeVille"]; ?>" > <?php echo $ville["nomVille"]; ?></option>

    <?php endforeach; ?>

</select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon bgColorTheme minTextBox">Date Début</span>
            <input type="text" class="form-control" name="dateDebut" id="dateDebut" size="10" 
                   maxlength="10" pattern="^[-,'°çéèù0-9a-zA-Z\/s]{3,45}$" title="Saisir une date" required>
        </div>
    </div>
</div>    
<div class="row">
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon bgColorTheme minTextBox">Date Fin</span>
            <input type="text" class="form-control" name="dateFin" id="dateFin" size="10" 
                   maxlength="20" pattern="^[-,'°çéèù0-9a-zA-Z\/s]{3,45}$" title="Saisir une date" required>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon bgColorTheme minTextBox">Volume estimé</span>
            <input type="text" class="form-control" name="volumeEstime" id="volumeEstime" size="10" 
                   maxlength="10" pattern="{1,99}$" title="Saisir un volume estimé" required>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">  
        <input class="btn btn-primary btn-lg " type="submit" value="Valider">
    </div>
    <div class="col-lg-2">
        <input class="btn btn-primary btn-lg " type="reset" value="Annuler">
    </div>
</div>
</form>

<?php include_once '_fin.inc.php'; ?>