<?php

/**
 * _gestionBase.inc
 * 
 * PHP version 7
 *
 * Ce fichier regroupe les fonctions de gestion de la base de données "festival"
 * @author Pv
 * @version 1.0
 * @package Festival
 */

/**
 * Retourne un gestionnaire de connexion.
 *
 * Se connecte à la base de données "festival" du serveur de bases de données MYSQL et retourne un gestionnaire de connexion
 * 
 * @return PDO|null Un objet PDO en cas de succès, "null" en cas d'echec
 */
function gestionnaireDeConnexion() {
    $pdo = null;
    try {
        $pdo = new PDO('mysql:host=localhost;dbname=Tholdi', 'root', 'aqsd-6', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    } catch (PDOException $err) {
        $messageErreur = $err->getMessage();
        error_log($messageErreur, 0);
    }
    return $pdo;
}

function creerReservation($codeVilleReserver, $codeVilleRendre, $code , $dateDebutReservation,$dateFinReservation, $volumeEstime) {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {  
        $codeVilleReserver = $pdo->quote($codeVilleReserver);
        $codeVilleRendre = $pdo->quote($codeVilleRendre);
        $code =$pdo->quote($code);
        $dateDebutReservation = $pdo->quote($dateDebutReservation);
        $dateFinReservation = $pdo->quote($dateFinReservation);
        $volumeEstime = $pdo->quote($volumeEstime);


        $req = "insert into RESERVATION (codeVilleReserver,codeVilleRendre,code,dateDebutReservation,dateFinReservation,volumeEstime)" .
                "values ($codeVilleReserver, $codeVilleRendre, 5 , $dateDebutReservation,$dateFinReservation, $volumeEstime)";
        $pdo->exec($req);
    }
}

function listeVille() {
    $lesVilles = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = 'select * from VILLE';
        $pdoStatement = $pdo->query($req);
        $lesVilles = $pdoStatement->fetchAll();
    }
    return $lesVilles;
}

function listeContainer() {
    $lesContainers = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = 'select * from RESERVER';
        $pdoStatement = $pdo->query($req);
        $lesContainers = $pdoStatement->fetchAll();
    }
    return $lesContainers;
}
function listeReservation() {
    $lesReservations = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = 'select * from RESERVATION';
        $pdoStatement = $pdo->query($req);
        $lesReservations = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $lesReservations;
}
?>