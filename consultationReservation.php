<?php
include_once '_debut.inc.php';
?>


<!-- Une div contenant la class "container" préfixe obligatoirement les lignes (div de class=row) -->
<div class="container">
    <!-- ligne principale -->
    <div class="row "> 
        <!-- première colonne (s'étend sur de 3 colonnes sur 12 possibles) -->
        <div class="col-md-3 border">
            <br />
            <div id="menuGauche" class="btn-group-vertical btn-block">

                <a href="consultationReservation.php" class="btn btn-primary ">
                    CONSULTER</a>
                <a href="creerReservation.php" class="btn btn-primary  ">
                    AJOUTER</a>

                <a href="#" class="btn btn-primary btn-block">
                    RECHERCHER</a>
            </div> 
            <img src="img/clefmusique.gif" class="img-responsive" alt="Responsive image">
        </div>

        <!-- deuxième colonne (s'étend sur 7 colonnes sur 12 possibles à partir de la 3) -->
        <div class="col-md-7 border">   
            <br />
            <!-- une ligne dans une colonne -->
            <div class="row">
                        <?php
                        $lesReservation = listeReservation();
                        foreach ($lesReservation as $reservation):
                        ?>

                    <div class="col-md-6">
                        <article class="panel panel-default articleEtablissement bgColorTheme">
                                <p> Code : <?php echo $reservation ["codeReservation"]?>   </p>
                                <p> Date début :<?php echo $reservation["dateDebutReservation"] ?>  </p>
                                <p> Date fin :<?php echo $reservation["dateFinReservation"] ?>  </p>
                                <p> Volume estimé :<?php echo $reservation ["volumeEstime"] ?> </p>
                                <p> Ville réserver :<?php echo $reservation ["codeVilleReserver"] ?> </p>
                                    <p> Ville arrivé :<?php echo $reservation ["codeVilleRendre"] ?> </p>
                        </article>
                    </div>

                    <?php endforeach; ?>



            </div>
        </div>
    </div>
    <hr>

    <footer>
        <p>&copy; Jules Ferry 2015</p>
    </footer>
</div> <!-- /container -->




<?php include("_fin.inc.php"); ?>
